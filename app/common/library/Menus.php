<?php

namespace app\common\library;

use app\common\model\system\AdminRules;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

/**
 * 菜单规则类
 *
 */
class Menus
{

    /**
     * 创建菜单
     * @param array $menuArray
     * @param string $note
     * @param mixed $parent 父类的name或pid
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function create(array $menuArray = [], string $note = '', $parent = 0)
    {
        if (!is_numeric($parent)) {
            $parentRule = AdminRules::getByTitle($parent);
            $pid = $parentRule ? $parentRule['id'] : 0;
        } else {
            $pid = $parent;
        }


        $allow = array_flip(['title', 'router', 'alias', 'type', 'icon', 'note', 'status']);

        foreach ($menuArray as $value) {

            // 子分类数据
            $data = array_intersect_key($value, $allow);
            $data['pid'] = $pid;
            $children = isset($value['children']) && $value['children'];

            if (!isset($value['type'])) {
                $data['type'] = $children ? 0 : 1;
            } else {
                $data['type'] = $value['type'];
            }
            if (!isset($value['status'])) {
                $data['status'] = 'normal';
            }

            // PLUGIN 标识
            $data['note'] = $note;
            $data['auth'] = $value['auth'] ?? 1;
            $data['isSystem'] = $value['isSystem'] ?? 0;

            if (!isset($value['sort'])) {
                $data['sort'] = AdminRules::count() + 1;
            } else {
                $data['sort'] = $value['sort'];
            }

            $data['alias'] = substr(str_replace('/', ':', $data['router']), 1);

            // 查询当前菜单
            $menu = AdminRules::where(['note' => $data['note'], 'pid' => $data['pid'], 'router' => $data['router']])->find();
            if (empty($menu)) {
                $menu = AdminRules::create($data);
            } else {
                $menu->where('id', $menu->id)->update($data);
            }

            if ($children) {
                self::create($value['children'], $note, $menu['id']);
            }
        }
    }

    /**
     * 启用菜单
     * @param string $name
     * @return boolean
     */
    public static function enable(string $name): bool
    {
        $ids = self::getAuthRuleIdsByName($name);
        if (!$ids) {
            return false;
        }
        AdminRules::where('id', 'in', $ids)->update(['status' => 'normal']);
        return true;
    }

    /**
     * 禁用菜单
     * @param string $name
     * @return boolean
     */
    public static function disable(string $name): bool
    {
        $ids = self::getAuthRuleIdsByName($name);
        if (!$ids) {
            return false;
        }
        AdminRules::where('id', 'in', $ids)->update(['status' => 'hidden']);
        return true;
    }

    /**
     * 导出指定名称的菜单规则
     * @param string $name
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function export(string $name): array
    {
        $menuList = AdminRules::field('id,pid,title,router,icon,auth')->where('note', $name)->select()->toArray();
        return self::delIdPid(list_to_tree($menuList));
    }

    /**
     * 删除菜单
     * @param string $name 规则name
     * @return boolean
     */
    public static function delete(string $name): bool
    {
        $ids = self::getAuthRuleIdsByName($name, strpos($name, '/') !== false);

        if (!$ids) {
            return false;
        }

        AdminRules::destroy($ids, true);
        return true;
    }

    /**
     * 根据名称获取规则IDS
     * @param string $name
     * @param bool $like
     * @return array
     */
    public static function getAuthRuleIdsByName(string $name, bool $like = false): array
    {
        $where[] = !$like ? ['note', '=', $name] : ['router', 'like', '%' . $name . '%'];
        return AdminRules::where($where)->column('id');
    }

    /**
     * 递归删除
     * @param $menuList
     * @return array
     */
    private static function delIdPid($menuList): array
    {
        foreach ($menuList as $key => $value) {
            unset($menuList[$key]['id']);
            unset($menuList[$key]['pid']);
            if (isset($value['children'])) {
                $menuList[$key]['children'] = self::delIdPid($value['children']);
            }
        }
        return $menuList;
    }

} 